﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;



namespace MPApi.Common
{
    public class BaseConfig
    {
      

        private static string _connectionString = null;
        public static string BaseConnectionString
        {
            get
            {
                if (_connectionString == null)
                {

                    // fetch connectionstring from the config file
                    _connectionString = ConfigurationManager.ConnectionStrings["1mp"].ConnectionString;
                }
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }

        

        
    }          
       
}     