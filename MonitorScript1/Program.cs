﻿using MPApi.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;
using System.Net.Mime;

namespace MonitorScript1
{
    class Program
    {
        //private int idvalue;
        static void Main(string[] args)
        {
            
                int[] intArgs = new int[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    if (!int.TryParse(args[i], out intArgs[i]))
                    {
                        Console.WriteLine($"All parameters must be integers. Could not convert {args[i]}");
                        return;
                    }
                    else
                    {
                    try
                    {
                        DataTable dt = new DataTable();
                        dt = Getdata(intArgs[i]).Tables[0];
                        
                        if (dt.Rows.Count > 0)
                        {
                            GetEmbeddedCSVValue(dt, intArgs[i]);
                        }
                        else
                        {
                            Console.Write("Returned No values");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Entered Value is not Present in the table");
                    }
                        


                    }
                }
           
             

            


        }

       

        public static void GetEmbeddedCSVValue(DataTable dt,int id)
        {
            SqlConnection con = new SqlConnection(BaseConfig.BaseConnectionString);
            try
            {
               
                string sql = "select embeddedCsv,emailAddress,emailBody,emailSubject from MonitoringScript where Id= " + id + "";
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                DataTable dt2 = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                ad.Fill(dt2);
                if(dt2.Rows.Count>0)
                {
                    Boolean EmbeddedCSVresult = dt2.Rows[0].Field<Boolean>(0);
                    string emailAddress = dt2.Rows[0]["emailAddress"].ToString();

                    string emailSubject = dt2.Rows[0]["emailSubject"].ToString();
                    if (EmbeddedCSVresult == true)
                    {
                        Console.Write("Sending Email..");
                        Console.Write(Environment.NewLine);

                        string emailBody = dt2.Rows[0]["emailBody"].ToString();
                     
                        if (dt != null && dt.Rows.Count > 0)
                        {
                        
                                var writer = new StringWriter();
                            
                                using (var csv = new CsvWriter(writer))
                                {
                                    foreach (DataColumn column in dt.Columns)
                                    {
                                        csv.WriteField(column.ColumnName);
                                    }
                                    csv.NextRecord();

                                    foreach (DataRow row in dt.Rows)
                                    {
                                        for (var i = 0; i < dt.Columns.Count; i++)
                                        {
                                            csv.WriteField(row[i]);
                                        }
                                        csv.NextRecord();
                                    }


                                }

                         
                            MemoryStream stream1 = new MemoryStream(Encoding.ASCII.GetBytes(writer.ToString()));
                                                         
                                Attachment attachment = new Attachment(stream1, new ContentType("text/csv"));
                                attachment.Name = "DataMonitor.csv";


                                Configuration oConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                                var mailSettings = oConfig.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

                                var message = new MailMessage();
                                string[] Multiple = emailAddress.Split(';');
                          
                                foreach (string multiple_email in Multiple)
                                {
                                    message.To.Add(new MailAddress(multiple_email));
                                }
                                message.From = new MailAddress(mailSettings.Smtp.From);
                               
                                message.Subject = emailSubject.ToString();
                                string body = emailBody;
                                body = body.Replace("{{Body}}","");
                                message.Body = body;
                               
                                message.IsBodyHtml = true;
                                message.Attachments.Add(attachment);
                               
                                try
                                {
                                    using (var smtp = new SmtpClient())
                                    {
                                     smtp.Send(message);
                                        Console.Write("Mail sent..");

                                    }
                                }
                                catch (Exception ex)
                                {
                                   
                                    Console.Write("Failed Sending..");
                                }
                       

                            
                        }
                    }
                    else
                    {
                        Console.Write("Sending Email..");
                        Console.Write(Environment.NewLine);

                        if (dt != null && dt.Rows.Count > 0)
                        {
                             var writer = new StringWriter();

                            using (var csv = new CsvWriter(writer))
                            {
                                foreach (DataColumn column in dt.Columns)
                                {
                                    csv.WriteField(column.ColumnName);
                                }
                                csv.NextRecord();

                                foreach (DataRow row in dt.Rows)
                                {
                                    for (var i = 0; i < dt.Columns.Count; i++)
                                    {
                                        csv.WriteField(row[i]);
                                    }
                                    csv.NextRecord();
                                }


                            }


                            string emailBody1 = dt2.Rows[0]["emailBody"].ToString();
                            string body = emailBody1;
                          
                            body = body.Replace("{{Body}}", writer.ToString());
                          
                            Configuration oConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                            var mailSettings = oConfig.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

                            var message = new MailMessage();
                            string[] Multiple = emailAddress.Split(';');
                           
                            foreach (string multiple_email in Multiple)
                            {
                                message.To.Add(new MailAddress(multiple_email));
                            }
                            message.From = new MailAddress(mailSettings.Smtp.From);

                            message.Subject = emailSubject.ToString();
                            message.Body = body;
                         
                            message.IsBodyHtml = true;
                            try
                            {
                                using (var smtp = new SmtpClient())
                                {
                                    smtp.Send(message);
                                    Console.Write("Mail sent..");

                                }
                            }
                            catch (Exception ex)
                            {

                                Console.Write("Failed Sending..");
                            }
                        }
                        
                    
                    }
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                con.Close();
            }
           
                        

        }

        
       

      

        private static DataSet Getdata(int id)
        {
            DataSet oDs = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                string sSQL = "exec proc_monitoringscript '" + id + "'";
                  return GetDataSetDB(sSQL);
               
            }
            catch (Exception e)
            {
               
                return null;
            }
        }

        private static DataSet GetDataSetDB(string strSql)
        {
            DataSet Dst = new DataSet();
            SqlConnection con = new SqlConnection(BaseConfig.BaseConnectionString);
            con.Open();
            try
            {
                              
                var cmd = new SqlCommand(strSql, con);
                var objDataAdapter = new SqlDataAdapter(strSql,con);
                objDataAdapter.Fill(Dst);

            }
            catch (Exception ex)
            {
              
                return null;
            }
            finally
            {
                con.Close();
            
            }
            return Dst;
        }

    }
}
